# Simple symfony environnement for dev with Docker 

**ONLY for DEV, not for production**

 A very simple but smart Dockerconfiguration to build a Symfony web application 

# 1 Run Locally
## Create a volume for database
```bash
  docker volume create docker-symfony-db-volume
```

## Clone this project - https command
```bash
  git clone https://gitlab.com/guillaume-goubel/docker-symfony-template.git
```

## Open this project  
```bash
  cd docker-symfony-template
```

## Optional : change git credentials for build image
git credentials are by default => cf. ARG in docker/php-symfony/Dockerfile    

## Run docker-compose
```bash
  docker compose up -d --build
```  

## launch interactive session with docker-symfony-environnement container
```bash
  docker exec -it docker-symfony-environnement bash
```

# 2 In docker-symfony-environnement container
## Create your Symfony application
tip: change your version.here symfony 7
```bash
  symfony new symfony-app --version="7.*" --webapp
```

## Launch the symfony server in daemon mode
```bash
  cd symfony-app
  symfony serve -d
```
*Symfony application is now available at http://127.0.0.1:9000 !*

## If you need a database, modify the .env file in symfony project like this example:  
note: db credentials are in .env file next to the docker-compose 
```bash
  DATABASE_URL="postgresql://symfony:replaceme@database:5432/app?serverVersion=13&charset=utf8"
```
*Adminer is now available at http://127.0.0.1:9001*  
note: for adminer => credentials are in the .env file nex docker compose file  
- system : (in this case) PostgreSQL
- server : db
- user : symfony
- password : replaceme
- dbname : app

tip: !!! don't forget to switch to your system choice, here "PostgreSQL" !!!

## MailDev  
note: smtp port is in config.ini in docker/php-symfony  
*MailDev is now available at http://127.0.0.1:9002*

# 3 Now ?
- Modify your symfony application with youre favorite IDE  
- Dont forget to do composer require and maker inside the docker-symfony-environnement container, not in youre local !!  
- Have fun !!  

# 4 Infos
## writing right problems
After the first time the Symfony project is installed, if you encounter problems with writing rights: stop and restart Docker Compose.
```bash
  docker compose down
  docker compose up -d
```
## Ready to use with
This docker-compose provides you :

- PHP-8.*-cli
    - Composer
    - Symfony CLI
    - Nodejs, npm, yarn
    - Some other php extentions
    - Linux base tools : grep, tree, nano, bash completion ... (edit Dockerfile for less or more...)
- postgres
- adminer
- mailDev
*there is a sript : scriptaddLocalUserInContainer for auto creation of user in container.By default user in container = localUser*

## Requirements
- Linux operating system, provide adaptations for a Mac or Windows environment.
- Docker
- Docker-compose

## Author
- guillaume goubel [@guillaume-goubel] (http://guillaume-goubel.fr)
- inspired by [@yoanbernabeu](https://github.com/yoanbernabeu)
