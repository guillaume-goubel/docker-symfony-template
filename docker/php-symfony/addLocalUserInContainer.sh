#!/bin/bash

# Rend le script exécutable
chmod +x "$0"

# Vérifie si l'utilisateur existe déjà
if ! id -u "$USER" > /dev/null 2>&1; then
    # Crée l'utilisateur avec le même nom que l'utilisateur local
    adduser --disabled-password --gecos "" "$USER"
fi

# Donne à l'utilisateur les droits sur le répertoire cible
chown -R "$USER:$USER" /var/www/html